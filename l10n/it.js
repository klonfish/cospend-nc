OC.L10N.register(
    "cospend",
    {
    "Cospend" : "Cospend",
    "Who pays?" : "Chi paga?",
    "To whom?" : "A chi?",
    "How much?" : "Quanto?",
    "Member name" : "Nome del membro",
    "Paid" : "Pagato",
    "Spent" : "Speso",
    "Balance" : "Saldo",
    "Saved Cospend setting" : "Salvato impostazioni Cospend",
    "Failed to save Cospend setting" : "Impossibile salvare l'impostazione Cospend",
    "Failed to add external project" : "Impossibile aggiungere il progetto esterno",
    "Failed to get external project" : "Impossibile ottenere il progetto esterno",
    "Failed to create project" : "Impossibile creare un progetto",
    "Created member {name}" : "Creato membro {name}",
    "Failed to add member" : "Impossibile aggiungere il membro",
    "Reactivate" : "Riattiva",
    "Deactivate" : "Disattiva",
    "Saved member" : "Membro salvato",
    "Failed to save member" : "Impossibile salvare il membro",
    "Bill created" : "Spesa creata",
    "Failed to create bill" : "Impossibile creare la spesa",
    "Saved bill" : "Spesa salvata",
    "Failed to save bill" : "Impossibile salvare la spesa",
    "link" : "collega",
    "Failed to delete external project" : "Impossibile eliminare il progetto esterno",
    "Failed to save external project" : "Impossibile salvare il progetto esterno",
    "Deleted {name}" : "Eliminato {name}",
    "Saved project" : "Progetto salvato",
    "Failed to save project" : "Impossibile salvare il progetto",
    "Deleted project {id}" : "Progetto eliminato {id}",
    "Failed to delete project" : "Impossibile eliminare il progetto",
    "No bill yet" : "Ancora nessuna spesa",
    "Deleted bill" : "Spesa eliminata",
    "Failed to delete bill" : "Impossibile eliminare la spesa",
    "Failed to get projects" : "Impossibile ottenere i progetti",
    "Failed to get statistics" : "Impossibile ottenere le statistiche",
    "Failed to get settlement" : "Impossibile ottenere la liquidazione",
    "Settlement of project {name}" : "Liquidazione del progetto {name}",
    "Export" : "Esporta",
    "Add these payments to project" : "Aggiungi questi pagamenti al progetto",
    "MoneyBuster link/QRCode for project {name}" : "Link di MoneyBuster/codice QR per il progetto {name}",
    "Scan this QRCode with an Android phone with MoneyBuster installed and open the link or simply send the link." : "Scansiona questo codice QR con un telefono Android con MoneyBuster installato e apri il link o semplicemente invia il link.",
    "Statistics of project {name}" : "Statistiche del progetto {name}",
    "Failed to get bills" : "Impossibile ottenere le spese",
    "Bill : {what}" : "Spesa : {what}",
    "What?" : "Cosa?",
    "Who payed?" : "Chi ha pagato?",
    "When?" : "Quando?",
    "For whom?" : "Per chi?",
    "All" : "Tutti",
    "None" : "Nessuno",
    "Create bills" : "Crea spesa",
    "Attach public link to personal file" : "Allega un link pubblico al file personale",
    "Classic, even split" : "Classico, diviso in parti uguali",
    "Classic mode: Choose a payer, enter a bill amount and select who is concerned by the whole spending, the bill is then split equitably between selected members. Real life example: One person pays the whole restaurant bill and everybody agrees to evenly split the cost." : "Modo classico: Scegli un pagatore, inserisci l'importo della spesa e seleziona chi è coinvolto nell' intera spesa, la fattura viene poi divisa in parti uguali tra i membri selezionati. Esempio di vita reale: Una persona paga l'intera fattura del ristorante e tutti sono d'accordo a dividere equamente il costo.",
    "Custom owed amount per member" : "Importo dovuto per membro",
    "Custom mode, uneven split: Choose a payer, ignore the bill amount (which is disabled) and enter a custom owed amount for each member who is concerned. Then press \"Create the bills\". Multiple bills will be created. Real life example: One person pays the whole restaurant bill but there are big price differences between what each person ate." : "Modalità personalizzata, divisione non uniforme: Scegli un pagatore, ignora l'importo della spesa (che è disabilitato) e inserisci un importo personalizzato dovuto per ogni membro interessato. Dopo premi \"Crea le spese\". Verranno create più spese. Esempio di vita reale: una persona paga l'intera spesa del ristorante, ma ci sono grande differenze di prezzo tra ciò che ha mangiato ogni persona.",
    "Even split with optional personal parts" : "Diviso in parti uguali con parti personali opzionali",
    "Classic+personal mode: This mode is similar to the classic one. Choose a payer and enter a bill amount corresponding to what was actually payed. Then select who is concerned by the bill and optionally set an amount related to personal stuff for some members. Multiple bills will be created: one for the shared spending and one for each personal part. Real life example: We go shopping, part of what was bought concerns the group but someone also added something personal (like a shirt) which the others don't want to collectively pay." : "Modalità classica+personale: questa modalità è simile a quella classica. Scegli un pagatore e inserisci un importo corrispondente a quello che è stato effettivamente pagato. Poi seleziona chi è coinvolto dalla spesa, facoltativamente, imposta un importo relativo alle cose personali per alcuni membri. Verranno creati più spese: una per la spesa condivisa e una per ogni parte personale. Esempio di vita reale: andiamo a fare shopping, una parte di ciò che è stato acquistato riguarda il gruppo ma qualcuno ha aggiunto qualcosa di personale (come una camicia) che gli altri non vogliono pagare collettivamente.",
    "Bill type" : "Tipo di spesa",
    "Repeat this bill every" : "Ripeti questa spesa ogni",
    "do not repeat" : "non ripetere",
    "day" : "giorno",
    "week" : "settimana",
    "month" : "mese",
    "year" : "anno",
    "Member list is not up to date. Reloading in 5 sec." : "La lista dei membri non è aggiornata. Ricarica tra 5 sec.",
    "Failed to update balances" : "Aggiornamento del saldo fallito",
    "Add member" : "Aggiungi membro",
    "Guest access link" : "Link di accesso ospite",
    "Rename" : "Rinomina",
    "Change password" : "Cambia password",
    "Display statistics" : "Mostra statistiche",
    "Settle the project" : "Liquidazione del progetto",
    "Export to csv" : "Esporta in csv",
    "Delete remote project" : "Elimina progetto remoto",
    "Delete" : "Elimina",
    "Link/QRCode for MoneyBuster" : "Link/codice QR per MoneyBuster",
    "Remove" : "Rimuovi",
    "Press enter to validate" : "Premi invio per convalidare",
    "Change weight" : "Modifica peso",
    "Bill values are not valid" : "I valori della spesa non sono validi",
    "Failed to save option values" : "Impossibile salvare i valori dell'opzione",
    "Failed to restore options values" : "Impossibile ripristinare i valori delle opzioni",
    "Failed to get user list" : "Impossibile ottenere la lista utente",
    "Shared project {pname} with {uname}" : "Progetto condiviso {pname} con {uname}",
    "Failed to add user share" : "Impossibile aggiungere la condivisione utente",
    "Failed to delete user share" : "Impossibile eliminare la condivisione utente",
    "Shared project {pname} with group {gname}" : "Progetto condiviso {pname} con gruppo {gname}",
    "Failed to add group share" : "Impossibile aggiungere la condivisione gruppo",
    "Failed to delete group share" : "Impossibile eliminare la condivisione gruppo",
    "Failed to generate public link to file" : "Impossibile generare il link pubblico al file",
    "Project exported in {path}" : "Progetto esportato in {path}",
    "Failed to export project" : "Esportazione progetto fallita",
    "Project statistics exported in {path}" : "Statistiche del progetto esportate in {path}",
    "Failed to export project statistics" : "Impossibile esportare le statistiche del progetto",
    "Project settlement exported in {path}" : "Liquidazione progetto esportata in {path}",
    "Failed to export project settlement" : "Impossibile esportare la liquidazione del progetto",
    "Project settlement bills added" : "Aggiunto i pagamenti di liquidazione del progetto",
    "Failed to add project settlement bills" : "Impossibile aggiungere i pagamenti di liquidazione del progetto",
    "Only CSV files can be imported" : "Solo i file CSV possono essere importati",
    "Failed to import project file" : "Impossibile importare il file del progetto",
    "Personal parts are bigger than the paid amount" : "Le parti personali sono più grandi dell'importo pagato",
    "Invalid values" : "Valori non validi",
    "There is no custom amount" : "Non c'è nessun importo personalizzato",
    "newMemberName" : "Nome nuovo membro",
    "New Bill" : "Nuova spesa",
    "2 active members are required to create a bill" : "2 membri attivi sono necessari per creare una spesa",
    "Guest link for '{pid}' copied to clipboard" : "Link ospite per '{pid}' copiato negli appunti",
    "Guest link copied to clipboard" : "Link ospite copiato negli appunti",
    "Choose file" : "Scegli file",
    "Choose csv project file" : "Scegli il file del progetto csv",
    "User \"%s\" shared Cospend project \"%s\" with you." : "L'utente \"%s\" ha condiviso il progetto Cospend \"%s\" con te.",
    "User \"%s\" stopped sharing Cospend project \"%s\" with you." : "L'utente \"%s\" ha smesso di condividere il progetto Cospend \"%s\" con te.",
    "Allow guests to create projects" : "Consenti agli ospiti di creare progetti",
    "Enabling this option will allow any visitor to create a project without having to log in." : "Abilitando questa opzione, qualsiasi visitatore potrà creare un progetto senza dover accedere.",
    "This can be useful if you are in a local network and want to let anybody create a project." : "Questo può essere utile se sei in una rete locale e vuoi permettere a chiunque di creare un progetto.",
    "Or maybe you are very generous and want to allow the entire world to use Cospend without having an account on your Nextcloud instance." : "O forse sei molto generoso e vuoi permettere all'intero mondo di usare Cospend senza avere un account sulla tua istanza Nextcloud.",
    "Authentication" : "Autenticazione",
    "Wrong project Id or password" : "Id progetto o password errati",
    "Project ID" : "ID Progetto",
    "Project password (aka Access code)" : "Password del progetto (o codice di accesso)",
    "New project" : "Nuovo progetto",
    "project id" : "id progetto",
    "myProjectId" : "mioIdProgetto",
    "name" : "nome",
    "My project name" : "Nome del mio progetto",
    "password (optional)" : "password (opzionale)",
    "Add project" : "Aggiungi progetto",
    "New bill" : "Nuova spesa",
    "Settings" : "Impostazioni",
    "Add external project" : "Aggiungi progetto esterno",
    "Nextcloud address" : "Indirizzo Nextcloud",
    "https://other.nextcloud.instance.net" : "https://other.nextcloud.instance.net",
    "password" : "password",
    "Add" : "Aggiungi",
    "Import csv project file" : "Importa file di progetto csv"
},
"nplurals=2; plural=(n != 1);");
