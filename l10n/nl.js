OC.L10N.register(
    "cospend",
    {
    "Cospend" : "Samen betalen",
    "Who pays?" : "Wie betaalt er?",
    "To whom?" : "Aan wie?",
    "How much?" : "Hoeveel?",
    "Member name" : "Naam van deelnemer",
    "Paid" : "Betaald",
    "Spent" : "Uitgegeven",
    "Balance" : "Saldo",
    "Saved Cospend setting" : "Instelling voor samen betalen opgeslagen",
    "Failed to save Cospend setting" : "Kan instelling niet opslaan",
    "Failed to add external project" : "Kan niet toevoegen aan extern project",
    "Failed to get external project" : "Kan extern project niet ophalen",
    "Failed to create project" : "Kan project niet creëren",
    "Created member {name}" : "Deelnemer '{name}' gecreëerd",
    "Failed to add member" : "Kan deelnemer niet toevoegen",
    "Reactivate" : "Heractiveren",
    "Deactivate" : "Deactiveren",
    "Saved member" : "Opgeslagen deelnemer",
    "Failed to save member" : "Kan deelnemer niet opslaan",
    "Bill created" : "Rekening gecreëerd",
    "Failed to create bill" : "Kan rekening niet creëren",
    "Saved bill" : "Opgeslagen rekening",
    "Failed to save bill" : "Kan rekening niet opslaan",
    "link" : "link",
    "Failed to delete external project" : "Kan extern project niet verwijderen",
    "Failed to save external project" : "Kan extern project niet opslaan",
    "Deleted {name}" : "'{name}' is verwijderd",
    "Saved project" : "Project opgeslagen",
    "Failed to save project" : "Kan project niet opslaan",
    "Deleted project {id}" : "Project '{id}' verwijderd",
    "Failed to delete project" : "Kan project niet verwijderen",
    "No bill yet" : "Nog geen rekening",
    "Deleted bill" : "Rekening verwijderd",
    "Failed to delete bill" : "Kan rekening niet verwijderen",
    "Failed to get projects" : "Kan projecten niet ophalen",
    "Failed to get statistics" : "Kan statistieken niet ophalen",
    "Failed to get settlement" : "Kan schikking niet ophalen",
    "Settlement of project {name}" : "Schikking van project '{name}'",
    "Export" : "Exporteren",
    "Add these payments to project" : "Voeg deze betalingen toe aan het project",
    "MoneyBuster link/QRCode for project {name}" : "MoneyBuster-link/QR-code van project '{name}'",
    "Scan this QRCode with an Android phone with MoneyBuster installed and open the link or simply send the link." : "Scan deze QR-code met een Android-toestel waarop MoneyBuster geïnstalleerd is en open de link, of verstuur de link.",
    "Statistics of project {name}" : "Statistieken van project '{name}'",
    "Failed to get bills" : "Kan rekeningen niet ophalen",
    "Bill : {what}" : "Rekening: {what}",
    "What?" : "Wat?",
    "Who payed?" : "Wie heeft er betaald?",
    "When?" : "Wanneer?",
    "For whom?" : "Aan wie?",
    "All" : "Alles",
    "None" : "Geen",
    "Create bills" : "Rekeningen creëren",
    "Attach public link to personal file" : "Openbare link toevoegen aan persoonlijk bestand",
    "Repeat this bill every" : "Herhaal deze rekening elke",
    "do not repeat" : "niet herhalen",
    "day" : "dag",
    "week" : "week",
    "month" : "maand",
    "year" : "jaar",
    "Member list is not up to date. Reloading in 5 sec." : "De deelnemerslijst is niet bijgewerkt. Deze wordt herladen over 5 seconden.",
    "Failed to update balances" : "Kan saldi niet bijwerken",
    "Add member" : "Deelnemer toevoegen",
    "Guest access link" : "Link voor gasttoegang",
    "Rename" : "Naam wijzigen",
    "Change password" : "Wachtwoord wijzigen",
    "Display statistics" : "Statistieken tonen",
    "Settle the project" : "Project schikken",
    "Export to csv" : "Exporteren naar CSV",
    "Delete remote project" : "Extern project verwijderen",
    "Delete" : "Verwijderen",
    "Link/QRCode for MoneyBuster" : "Link/QR-code voor MoneyBuster",
    "Remove" : "Verwijderen",
    "Press enter to validate" : "Druk op enter te valideren",
    "Change weight" : "Gewicht aanpassen",
    "Bill values are not valid" : "Ongeldige rekeningwaarden",
    "Failed to save option values" : "Kan sommige opties niet opslaan",
    "Failed to restore options values" : "Kan sommige opties niet herstellen",
    "Failed to get user list" : "Kan deelnemerslijst niet ophalen",
    "Shared project {pname} with {uname}" : "Project '{pname}' is gedeeld met '{uname}'",
    "Failed to add user share" : "Kan niet delen met gebruiker",
    "Failed to delete user share" : "Kan deling niet verwijderen",
    "Failed to generate public link to file" : "Kan geen openbare link genereren naar bestand",
    "Project exported in {path}" : "Project is geëxporteerd naar {path}",
    "Failed to export project" : "Kan project niet exporteren",
    "Project statistics exported in {path}" : "Projectstatistieken zijn geëxporteerd naar {path}",
    "Failed to export project statistics" : "Kan projectstatistieken niet exporteren",
    "Project settlement exported in {path}" : "Projectschikking is geëxporteerd naar {path}",
    "Failed to export project settlement" : "Kan projectschikking niet exporteren",
    "Project settlement bills added" : "Projectschikking - rekeningen toegevoegd",
    "Failed to add project settlement bills" : "Kan geen rekeningen toevoegen",
    "Only CSV files can be imported" : "Je kunt alleen CSV-bestanden importeren",
    "Failed to import project file" : "Kan projectbestand niet importeren",
    "Invalid values" : "Ongeldige waarden",
    "There is no custom amount" : "Er is geen aangepast bedrag",
    "newMemberName" : "NieuweGebruikersnaam",
    "New Bill" : "Nieuwe rekening",
    "2 active members are required to create a bill" : "Er zijn 2 actieve deelnemers benodigd om een rekening te creëren",
    "Guest link for '{pid}' copied to clipboard" : "Gastlink voor '{pid}' is gekopieerd naar het klembord",
    "Guest link copied to clipboard" : "Gastlink is gekopieerd naar het klembord",
    "Choose file" : "Bestand kiezen",
    "Choose csv project file" : "Kies een CSV-projectbestand",
    "User \"%s\" shared Cospend project \"%s\" with you." : "De gebruiker '%s' heeft het Cospend-project '%s' met je gedeeld.",
    "User \"%s\" stopped sharing Cospend project \"%s\" with you." : "De gebruiker '%s' heeft het delen van het Cospend-project '%s' stopgezet.",
    "Allow guests to create projects" : "Gasten toestaan om projecten te creëren",
    "Enabling this option will allow any visitor to create a project without having to log in." : "Als je dit inschakelt, dan kan elke bezoeker een project creëren zonder in te loggen.",
    "This can be useful if you are in a local network and want to let anybody create a project." : "Dit kan nuttig zijn als je je op een lokaal netwerk bevindt en iedereen projecten wilt laten creëren.",
    "Or maybe you are very generous and want to allow the entire world to use Cospend without having an account on your Nextcloud instance." : "Of als je erg genereus bent en de gehele wereld Cospend wilt laten gebruiken zonder een account te creëren op je Nextcloud-instantie.",
    "Authentication" : "Authenticatie",
    "Wrong project Id or password" : "Project-ID of wachtwoord onjuist",
    "Project ID" : "Project-ID",
    "Project password (aka Access code)" : "Project-wachtwoord (ook wel: toegangscode)",
    "New project" : "Nieuw project",
    "project id" : "project-ID",
    "myProjectId" : "MijnProject-ID",
    "name" : "naam",
    "My project name" : "Mijn projectnaam",
    "password (optional)" : "wachtwoord (optioneel)",
    "Add project" : "Project toevoegen",
    "New bill" : "Nieuwe rekening",
    "Settings" : "Instellingen",
    "Add external project" : "Extern project toevoegen",
    "Nextcloud address" : "Nextcloud-adres",
    "https://other.nextcloud.instance.net" : "https://andere.nextcloud.instantie.net",
    "password" : "wachtwoord",
    "Add" : "Toevoegen",
    "Import csv project file" : "CSV-projectbestand importeren"
},
"nplurals=2; plural=(n != 1);");
