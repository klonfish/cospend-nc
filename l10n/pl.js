OC.L10N.register(
    "cospend",
    {
    "Cospend" : "Cospend",
    "Who pays?" : "Kto płaci?",
    "To whom?" : "Do kogo?",
    "How much?" : "Ile?",
    "Member name" : "Nazwa użytkownika"
},
"nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);");
