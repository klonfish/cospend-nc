# Nextcloud Cospend 💰

Nextcloud Cospend ist ein Gruppen/geteilter Budget Manager. [IHateMoney](https://github.com/spiral-project/ihatemoney/) diente hierbei als Vorbild.

Immer wenn du Geld mit anderen teilst, z. B. in einer WG oder im Urlaub mit Freunden, kannst du diese App verwenden.

Es lässt dich Projekte mit Mitgliedern und Ausgaben erstellen. Anhand der Ausgaben im Projekt wird für jedes Mitglied eine Bilanz erstellt. Somit ist ersichtlich, wer wem etwas schuldet. Letzendlich kannst du dir dann in der Abrechnung anzeigen lassen, welche Zahlungen zu leisten sind, um die Bilanzen der Mitglieder auszugleichen.

Projektmitglieder sind unabhängig von Nextcloud Benutzern. Sobald du Zugang zu einem Projekt hast (als Gast oder Nextcoud-Nutzer), kannst du ohne Einschränkungen hinzufügen/editieren/löschen. Projekte können von Personen ohne Nextcloud-Konto aufgerufen und geändert werden. Jedes Projekt hat eine ID und ein Passwort für den Gast-Zugriff.

Der [MoneyBuster](https://gitlab.com/eneiluj/moneybuster) Android-Client ist bei [F-Droid](https://f-droid.org/packages/net.eneiluj.moneybuster/) verfügbar.

## Funktionen

* ✎ erstelle/bearbeite/lösche Projekte, Mitglieder, Ausgaben
* ⚖ Mitglieder Bilanzen überprüfen
* 🗠 Projektstatistik anzeigen
* ♻ Abrechnungsplan anzeigen
* 🎇 Erstelle Ausgleichszahlungen vom Abrechnungsplan (automatisch)
* 🗓 Erstelle wiederkehrende Ausgaben (täglich/wöchentlich/monatlich/jährlich)
* 📊 Gib bei neuen Ausgaben einen eigenen Betrag für jedes Mitglied an (optional)
* 🔗 Füge der Ausgabenbeschreibung eine persönliche Datei mit einen öffentlichen Link hinzu (z.B. Bild der Quittung für eine Ausgabe)
* 👩 Gastzugang für Personen ohne Nextcloud-Konto
* 👫 Teile ein Projekt mit Nextcloud Benutzern
* 🖫 Importiere/Exportieren ein Projekt als CSV (kompatibel mit CSV-Dateien von IHateMoney)
* 🖧 externes Projekt hinzufügen (von einer anderen Nextcloud Instanz gehostet)
* 🔗 Link/QRCode generieren um Projekte einfach in MoneyBuster zu importieren

Diese App wurde mit Nextcloud 15 in Kombination mit Firefox 57+ und Chromium getestet.

Diese App befindet sich in der Entwicklung.

🌍 Hilf uns diese App auf [Nextcloud-Cospend/MoneyBuster Crowdin Projekt](https://crowdin.com/project/moneybuster) zu übersetzen.

⚒ Schau dir in den [Richtlinien zur Mitarbeit](https://gitlab.com/eneiluj/cospend-nc/blob/master/CONTRIBUTING.md) an, wie du uns zusätzlich helfen kannst.

## Installieren

Finde Details zur Installation in der [AdminDoc](https://gitlab.com/eneiluj/cospend-nc/wikis/admindoc).

Schau in das [CHANGELOG](https://gitlab.com/eneiluj/cospend-nc/blob/master/CHANGELOG.md#change-log) um zu sehen was gerade neu und zukünftig geplant ist.

Finde eine Liste aller Autoren in der [AUTHORS](https://gitlab.com/eneiluj/cospend-nc/blob/master/AUTHORS.md#authors) Datei.

## Bekannte Probleme

* Es macht dich nicht reich

Jedes Feedback ist willkommen.