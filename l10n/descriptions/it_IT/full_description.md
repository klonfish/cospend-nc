# Nextcloud Cospend 💰

Nextcloud Cospend è un budget manager condiviso/di gruppo. E' stato ispirato dal grande [IHateMoney](https://github.com/spiral-project/ihatemoney/).

Puoi usarlo quando condividi una casa, quando vai in vacanza con gli amici, ogni volta che condividi i soldi con gli altri.

Permette di creare progetti con membri e spese. Ogni membro ha un bilancio calcolato sulla base delle spese di progetto. In questo modo si può vedere chi deve al gruppo quanto e quanto deve il gruppo a chi. In ultimo, è possibile richiedere un piano di liquidazione che indica i pagamenti da effettuare per ripristinare i saldi dei membri.

I membri del progetto sono indipendenti dagli utenti Nextcloud. Una volta che hai accesso a un progetto (come ospite o come utente Nextcloud), non ci sono restrizione su ciò che puoi aggiungere/modificare/eliminare. I progetti sono accessibili e modificabili da persone senza un account Nextcloud. Ogni progetto ha un ID e una password per l'accesso ospite.

[MoneyBuster](https://gitlab.com/eneiluj/moneybuster) Il client Android è [disponibile in F-Droid](https://f-droid.org/packages/net.eneiluj.moneybuster/).

## Funzionalità

* ✎ crea/modifica/elimina progetti, membri, spese
* ⚖ controlla i saldi dei membri
* 🗠 visualizza le statistiche del progetto
* ♻ visualizza piano di liquidazione
* 🎇 crea automaticamente i pagamenti di rimborso dal piano di liquidazione
* 🗓 crea spese ricorrenti (giorno/settimana/mese/anno)
* 📊 facoltativamente fornisce un importo personalizzato per ciascun membro in nuove spese
* 🔗 inserisci il link pubblico a un file personale nella descrizione della fattura (immagine del scontrino per esempio)
* 👩 accesso ospite per persone fuori Nextcloud
* 👫 condividi un progetto con gli utenti Nextcloud
* 🖫 importa/esporta progetti come csv (compatibile con i file csv da IHateMoney)
* 🖧 aggiungi progetti esterni (ospitati da un'altra istanza Nextcloud)
* 🔗 genera link/codice QR per importare facilmente progetti in MoneyBuster

Questa app è stata testata su Nextcloud 15 con Firefox 57+ e Chromium.

Questa app è in fase di sviluppo.

🌍 Aiutaci a tradurre questa app su [Nextcloud-Cospend/MoneyBuster progetto Crowdin](https://crowdin.com/project/moneybuster).

⚒ Scopri altri modi per aiutare nelle linee guida per [contributi](https://gitlab.com/eneiluj/cospend-nc/blob/master/CONTRIBUTING.md).

## Installazione

Vedi gli [AdminDoc](https://gitlab.com/eneiluj/cospend-nc/wikis/admindoc) per i dettagli di installazione.

Controlla il file [CHANGELOG](https://gitlab.com/eneiluj/cospend-nc/blob/master/CHANGELOG.md#change-log) per vedere cosa c'è di nuovo e cosa sta arrivando nella prossima versione.

Controlla il file [AUTHORS](https://gitlab.com/eneiluj/cospend-nc/blob/master/AUTHORS.md#authors) per vedere l'elenco completo degli autori.

## Problemi noti

* non ti rende ricco

Qualsiasi feedback sarà apprezzato.