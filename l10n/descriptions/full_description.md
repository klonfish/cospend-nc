# Nextcloud Cospend 💰

Nextcloud Cospend is a group/shared budget manager.
It was inspired by the great [IHateMoney](https://github.com/spiral-project/ihatemoney/).

You can use it when you share a house, when you go on vacation with friends, whenever you share money with others.

It lets you create projects with members and bills. Each member has a balance computed from the project bills.
This way you can see who owes the group and who the group owes. Ultimately you can ask for a settlement plan telling you which payments to make to reset members balances.

Project members are independent from Nextcloud users.
Once you've got access to a project (as a guest or as a Nextcloud user), there is no restriction on what you can add/edit/delete.
Projects can be accessed and modified by people without a Nextcloud account. Each project has an ID and a password for guest access.

[MoneyBuster](https://gitlab.com/eneiluj/moneybuster) Android client is [available in F-Droid](https://f-droid.org/packages/net.eneiluj.moneybuster/).

## Features

* ✎ create/edit/delete projects, members, bills
* ⚖ check member balances
* 🗠 display project statistics
* ♻ display settlement plan
* 🎇 automatically create reimbursement bills from settlement plan
* 🗓 create recurring bills (day/week/month/year)
* 📊 optionally provide custom amount for each member in new bills
* 🔗 insert public link to a personal file in bill description (picture of physical bill for example)
* 👩 guest access for people outside Nextcloud
* 👫 share a project with Nextcloud users
* 🖫 import/export projects as csv (compatible with csv files from IHateMoney)
* 🖧 add external projects (hosted by another Nextcloud instance)
* 🔗 generate link/QRCode to easily import projects in MoneyBuster

This app is tested on Nextcloud 15 with Firefox 57+ and Chromium.

This app is under development.

🌍 Help us to translate this app on [Nextcloud-Cospend/MoneyBuster Crowdin project](https://crowdin.com/project/moneybuster).

⚒ Check out other ways to help in the [contribution guidelines](https://gitlab.com/eneiluj/cospend-nc/blob/master/CONTRIBUTING.md).

## Install

See the [AdminDoc](https://gitlab.com/eneiluj/cospend-nc/wikis/admindoc) for installation details.

Check [CHANGELOG](https://gitlab.com/eneiluj/cospend-nc/blob/master/CHANGELOG.md#change-log) file to see what's new and what's coming in next release.

Check [AUTHORS](https://gitlab.com/eneiluj/cospend-nc/blob/master/AUTHORS.md#authors) file to see complete list of authors.

## Known issues

* it does not make you rich

Any feedback will be appreciated.

