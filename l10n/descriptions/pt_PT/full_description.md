# Nextcloud Cospend 💰

Nextcloud Cospend é um gestor de orçamento de grupo/partilhado. Foi inspirado pelo grande [IHateMoney](https://github.com/spiral-project/ihatemoney/).

Pode usá-lo ao partilhar uma casa, quando for férias com amigos ou sempre que partilhe despesas com outros.

Permite-lhe criar projectos com membros e facturas. Cada membro tem um saldo calculado a partir das facturas do projecto. Desta forma pode ver quem deve ao grupo e a quem o grupo deve. Em última análise pode pedir um plano de liquidação que lhe diga quais os pagamentos a fazer para reiniciar os saldos dos membros.

Os membros do projecto são independentes dos utilizadores do Nextcloud. Uma vez que tenha acesso a um projecto (como convidado ou como utilizador do Nextcloud), não há restrição em relação ao que pode adicionar/editar/excluir. Os projectos podem ser acedidos e modificados por pessoas que não tenham conta no Nextcloud. Cada projecto tem ID e senha para dar acesso aos convidados.

[O cliente do MoneyBuster](https://gitlab.com/eneiluj/moneybuster) Android está [disponível no F-Droid](https://f-droid.org/packages/net.eneiluj.moneybuster/).

## Funcionalidades

* ✎ criar/editar/eliminar projectos, membros, facturas
* ⚖ verificar os saldos dos membros
* 🗠 exibir estatísticas do projecto
* ♻ exibir plano de liquidação
* 🎇 criar automaticamente facturas de reembolso a partir do plano de liquidação
* 🗓 criar facturas recorrentes (dia/semana/mês/ano)
* 📊 fornece, opcionalmente, um valor personalizado para cada membro em facturas novas
* 🔗 insere uma ligação pública para um ficheiro pessoal na descrição da factura (fotografia da factura em papel, por exemplo)
* 👩 acesso de convidado para pessoas fora do Nextcloud
* 👫 partilha de um projecto com utilizadores do Nextcloud
* 🖫 importação/exportação de projectos no formato CSV (compatível com ficheiros CSV do IHateMoney)
* 🖧 adição de projectos externos (alojados noutra instância Nextcloud)
* 🔗 gerar ligação/código-QR para importar facilmente projectos no MoneyBuster

Esta aplicação foi testada no Nextcloud 15 com o Firefox 57+ e o Chromium.

Esta aplicação está em desenvolvimento.

🌍 Ajude-nos a traduzir esta aplicação no [projecto Crowdin do Nextcloud-Cospend/MoneyBuster](https://crowdin.com/project/moneybuster).

⚒ Veja outras formas de ajudar nas [directrizes de contribuição](https://gitlab.com/eneiluj/cospend-nc/blob/master/CONTRIBUTING.md).

## Instalar

Consulte o [AdminDoc](https://gitlab.com/eneiluj/cospend-nc/wikis/admindoc) para obter detalhes de instalação.

Consulte o ficheiro [CHANGELOG](https://gitlab.com/eneiluj/cospend-nc/blob/master/CHANGELOG.md#change-log) para ver o que há de novo e o que virá no próximo lançamento.

Consulte o ficheiro [AUTHORS](https://gitlab.com/eneiluj/cospend-nc/blob/master/AUTHORS.md#authors) para ver a lista completa de autores.

## Problemas conhecidos

* não lhe traz riqueza

Qualquer comentário será apreciado.